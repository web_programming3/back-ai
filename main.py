from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List
import joblib

app = FastAPI()

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
model = joblib.load('laptop5.pkl')

class LaptopType(BaseModel):
    Cpu: int
    Ram: int
    Memory: int
    Gpu: int
    Price: int

class Laptop(BaseModel):
    TypeOfLaptop: str

# Function to make predictions
def predict_laptop_type(laptop_features: List[int]) -> str:
    try:
        prediction = model.predict([laptop_features])
        return str(prediction[0])
    except Exception as e:
        print("Error predicting laptop type: ", e)
        raise HTTPException(status_code=500, detail="Prediction failed")

@app.get("/laptop/options")
async def fetch_laptop_options():
    try:
        # Call function to get laptop options
        options = get_laptop_options()
        return options
    except Exception as e:
        # Log error
        print("Error fetching laptop options: ", e)
        # Raise HTTP exception for internal server error
        raise HTTPException(status_code=500, detail="Failed to fetch laptop options")
        
# Route to predict laptop type
@app.post("/laptop/predict/")
async def predict_laptops(lap: LaptopType):
    try:
        # Ensure input data is valid
        if lap.Cpu <= 0 or lap.Ram <= 0 or lap.Memory <= 0 or lap.Gpu <= 0 or lap.Price <= 0:
            raise HTTPException(status_code=400, detail="Invalid input data")

        # Make prediction
        laptop_features = [lap.Cpu, lap.Ram, lap.Memory, lap.Gpu, lap.Price]
        predicted_type = predict_laptop_type(laptop_features)
        return Laptop(TypeOfLaptop=predicted_type)
    except Exception as e:
        # Log error
        print("Error prediction: ", e)
        # Return HTTP 500 status code for internal server error
        raise HTTPException(status_code=500, detail="Prediction failed")
